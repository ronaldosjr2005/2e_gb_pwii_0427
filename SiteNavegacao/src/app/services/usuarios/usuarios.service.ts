import { Injectable } from '@angular/core';
import { Usuario } from './../../models/Usuario';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  private readonly URL_1 = "https://3000-zombieritua-2egbapi0810-3tu77xwwiaz.ws-us79.gitpod.io/"
  private readonly URL_2 = "https://3000-zombieritua-2egbapi0810-994mbd7lhan.ws-us78.gitpod.io/"
  private readonly URL_LOCAL = "http://localhost:3000/"
  private readonly URL = this.URL_LOCAL

  constructor(
    private readonly http: HttpClient
  ) { }

  logar(usuario: Usuario): Observable<any> {
    return this.http.post<any>(`${this.URL}usuario/login`, usuario)
  }

  cadastrar(usuario: Usuario): Observable<any> {
    return this.http.post<any>(`${this.URL}usuario`, usuario)
  }

  buscarUsuarios(): Observable<any> {
    return this.http.get<any>(`${this.URL}usuarios`)
  }
}
