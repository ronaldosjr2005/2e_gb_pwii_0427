import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LivrosService {
  private readonly URL_1 = "https://3000-zombieritua-2egbapi0810-3tu77xwwiaz.ws-us79.gitpod.io/"
  private readonly URL_2 = "https://3000-zombieritua-2egbapi0810-3tu77xwwiaz.ws-us77.gitpod.io/"
  private readonly URL = this.URL_2
  constructor(
    private http: HttpClient
  ) { }

  buscarlivros():Observable<any>{
    return this.http.get<any>(`${this.URL}livros`)
  }
}
