import { Pessoa } from './Pessoa'

export class Funcionario extends Pessoa {
    data_admissao: string = ""
    salario: number = 0
    cargo: string = ""
    setor: string = ""
}