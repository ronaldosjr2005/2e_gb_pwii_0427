import { LoginComponent } from './login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadUsuarioComponent } from '../../cad-usuario/cad-usuario.component';

const routes: Routes = [
  {
    path: "",
    component: LoginComponent
  },
  {
    path: "cad-usuario",
    component: CadUsuarioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
