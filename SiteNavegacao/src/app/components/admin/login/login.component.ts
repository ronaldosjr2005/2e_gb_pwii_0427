import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../../models/Usuario';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  usuario: Usuario
  mensagem: string
  

  constructor(
    private readonly usuariosService: UsuariosService,
    private readonly router: Router
  ) { 
    this.usuario = new Usuario()
    this.mensagem = ""
  }

  ngOnInit(): void {
  }
  
  logar(): void {
    console.log(this.usuario)
    this.usuariosService.logar(this.usuario).subscribe({
      next: (resposta) => {
        this.mensagem = ""
        this.router.navigateByUrl("/dashboard")
      },
      error: (erro) => {
        console.log(erro)
        this.mensagem = erro.error.mensagem
      }
    })
  }
}
