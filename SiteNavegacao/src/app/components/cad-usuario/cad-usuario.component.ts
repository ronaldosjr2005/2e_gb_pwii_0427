import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/models/Usuario';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-cad-usuario',
  templateUrl: './cad-usuario.component.html',
  styleUrls: ['./cad-usuario.component.scss']
})
export class CadUsuarioComponent implements OnInit {
  usuario: Usuario
  mensagem: string
  
  constructor(
    private readonly usuariosService: UsuariosService,
    private readonly router: Router
  ) { 
    this.usuario = new Usuario()
    this.mensagem = ""
  }

  ngOnInit(): void {
  }

  salvar(): void {
    this.usuariosService.cadastrar(this.usuario)
    .subscribe({
        next: (resposta) => {
            this.mensagem = resposta.mensagem
            
            setTimeout(
                () => {
                    this.voltarParaListaDeUsuarios()
                },
                2000
            )
        },
        error: (erro) => {
            console.error(erro)
        }
    })
  }
  
  voltarParaListaDeUsuarios(): void {
    this.router.navigateByUrl('/admin/usuarios')
  }
}
