import { Component, OnInit } from '@angular/core';
import { Autor } from 'src/app/models/Autor';
import { AutoresService } from 'src/app/services/autores/autores.service';

@Component({
  selector: 'app-cad-autor',
  templateUrl: './cad-autor.component.html',
  styleUrls: ['./cad-autor.component.scss']
})
export class CadAutorComponent implements OnInit {

  public autor: Autor

  constructor(
    private autoresService: AutoresService
  ) {
    this.autor = new Autor()
  }
    ngOnInit() {
    }
  
    cadastrar (): void {
      console.log(this.autor)
  
      this.autoresService.cadastrar(this.autor)
      .subscribe({
        next: (dados) => {
          console.log(dados)
        },
        error: (erro) => {
          console.error(erro)
        }
      })
    }
  
}
