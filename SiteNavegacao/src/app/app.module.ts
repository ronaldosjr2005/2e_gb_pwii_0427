import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadAutorComponent } from './components/cad-autor/cad-autor.component';
import { CadEditoraComponent } from './components/cad-editora/cad-editora.component';
import { CadLivroComponent } from './components/cad-livro/cad-livro.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { MenuComponent } from './components/commons/menu/menu.component';
import { LivrosService } from './services/livros/livros.service';
import { AutoresService } from './services/autores/autores.service';
import { UsuariosService } from './services/usuarios/usuarios.service';
import { CadUsuarioComponent } from './components/cad-usuario/cad-usuario.component';

@NgModule({
  declarations: [
    AppComponent,
    CadAutorComponent,
    CadEditoraComponent,
    CadLivroComponent,
    PageNotFoundComponent,
    MenuComponent,
    CadUsuarioComponent
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    LivrosService,
    AutoresService,
    UsuariosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
